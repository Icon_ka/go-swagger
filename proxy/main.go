package main

import (
    "github.com/go-chi/chi"
    "net/http"
    "encoding/json"
    "strings"
    "fmt"
    "github.com/go-chi/jwtauth"
    "golang.org/x/crypto/bcrypt"
    "net/http/httputil"
    "net/url"
    "log"
    "os"
    "text/template"
    "github.com/go-chi/chi/middleware"
)

const searchAfterLoginTemplate = `# Geo API
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" crossorigin=""/>

<p>Поиск адреса</p>
<input id="search" />

<div id="result"></div>

<div id="mapid" style="height: 50vh"></div>

<!-- Include Leaflet JavaScript -->
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" crossorigin=""></script>
<script>
    let startPos = [59.9311, 30.3609];
    var mymap = L.map('mapid').setView(startPos, 11);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; OpenStreetMap contributors',
        maxZoom: 18
    }).addTo(mymap);
    var currentMarker = null;
    // Обработчик события клика по карте
    mymap.on('click', function(e) {
        let data = {
            lat: e.latlng.lat.toString(),
            lon: e.latlng.lng.toString()
        };
        fetch('/api/address/geocode', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer {{.Token}}'
            },
            body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then(data => {
           table.setData(data.suggestions);
           if (data.suggestions.length > 0) {
                mymap.flyTo([data.suggestions[0].data.geo_lat, data.suggestions[0].data.geo_lon], 17);
                if (currentMarker) {
                    // Перемещение существующего маркера
                    currentMarker.setLatLng({lat: data.suggestions[0].data.geo_lat, lon: data.suggestions[0].data.geo_lon});
                } else {
                    // Создание нового маркера
                    currentMarker = L.marker({lat: data.suggestions[0].data.geo_lat, lon: data.suggestions[0].data.geo_lon}).addTo(mymap);
                }
           }
        })
        .catch(error => {
            console.log('Error:', error);
        });
    });
    // Сброс текущего маркера при двойном клике
    mymap.on('dblclick', function(e) {
        console.log('dblclick');
        if (currentMarker) {
            mymap.removeLayer(currentMarker);
            currentMarker = null;
        }
    });
</script>
<link href="https://unpkg.com/tabulator-tables@5.5.0/dist/css/tabulator.min.css" rel="stylesheet">
 <script type="text/javascript" src="https://unpkg.com/tabulator-tables@5.5.0/dist/js/tabulator.min.js"></script>
<script type="text/javascript">
//Build Tabulator
var tableData = [];
var table = new Tabulator("#result", {
    height:"311px",
    layout:"fitColumns",
    reactiveData:true, //turn on data reactivity
    responsiveLayout: "hide",
    data:tableData, //assign data to table
    placeholder:"No Data Set",
    selectable: true,
    autoColumns:true, //create columns from data field names
    rowClick:function(e, cell) {
        e.preventDefault();
        console.log("rowClick fired");
        e.stopPropagation();
    },
    selectableCheck:function(row){
        //row - row component
        let data = row.getData();
        if (data.lat != "" && data.lon != "") {
            mymap.flyTo([data.geo_lat, data.geo_lon], 17);
            if (currentMarker) {
                // Перемещение существующего маркера
                currentMarker.setLatLng({lat: data.geo_lat, lon: data.geo_lon});
            } else {
                // Создание нового маркера
                currentMarker = L.marker({lat: data.geo_lat, lon: data.geo_lon}).addTo(mymap);
            }
        }
        table.deselectRow();
        console.log("select fired");
        return true; //allow selection of rows where the age is greater than 18
    },
});
document.getElementById('search').addEventListener('input', function() {
    console.log('search change');
    if (this.value.length < 3) {
        return;
    }
    const data = {
        query: this.value
    };
    fetch('/api/address/search', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer {{.Token}}'
        },
        body: JSON.stringify(data)
    })
    .then(response => response.json())
    .then(data => {
       table.setData(data.suggestions);
       if (data.suggestions.length > 0) {
            mymap.flyTo([data.suggestions[0].data.geo_lat, data.suggestions[0].data.geo_lon], 17);
       }
    })
    .catch(error => {
        console.log('Error:', error);
    });
});
</script>`

type Token struct {
    Token string
}

// UserAuth это структура, представляющая тело JSON запроса регистрации и авторизации.
// swagger:model
type UserAuth struct {
    Username string `json:"username" binding:"required"`
    Password string `json:"password" binding:"required"`
}

// SearchRequest это структура, представляющая тело JSON поискового запроса.
// swagger:model
type SearchRequest struct {
    Query string `json:"query"`
}

// Address это структура, представляющая ответ JSON на запрос поиска или геокодирования.
// swagger:model
type Address struct {
    Suggestions []Suggestion `json:"suggestions"`
}

// Suggestion это структура, которая представляет одно предложение в ответе.
// swagger:model
type Suggestion struct {
    Value             string `json:"value"`
    UnrestrictedValue string `json:"unrestricted_value"`
    Data              Data   `json:"data"`
}

// Data это структура, которая представляет координаты точки.
// swagger:model
type Data struct {
    Lat string `json:"geo_lat"`
    Lon string `json:"geo_lon"`
}

// GeocodeRequest  это структура, представляющая тело JSON запроса геокодирования.
// swagger:model
type GeocodeRequest struct {
    Lat string `json:"lat"`
    Lon string `json:"lon"`
}

var pass string

var tokenAuth *jwtauth.JWTAuth

func main() {
    r := chi.NewRouter()
    tokenAuth = jwtauth.New("HS256", []byte("secret"), nil)

    proxy := NewReverseProxy("hugo", "1313")
    r.Use(proxy.ReverseProxy, middleware.Logger)

    r.Get("/swagger", swaggerUI)
    // swagger:route POST /api/login login
    // Авторизация.
    // responses:
    //   200: token
    r.Post("/api/login", apiLogin)

    // swagger:route POST /api/register register
    // Поиск регистрация.
    // responses:
    //   200: token
    r.Post("/api/register", apiRegister)

    r.Group(func(r chi.Router) {
        r.Use(jwtauth.Verifier(tokenAuth))
        r.Use(jwtauth.Authenticator)
        // swagger:route POST /api/address/search search searchRequest
        // Поиск адреса.
        // responses:
        //   200: address
        r.Post("/api/address/search", apiSearchHandler)
        // swagger:route POST /api/address/geocode geocode geocodeRequest
        // Геокодирование адреса.
        // responses:
        //   200: address
        r.Post("/api/address/geocode", apiGeocodeHandler)
    })

    http.ListenAndServe(":8080", r)
}

// apiRegister handles register requests.
// swagger:operation POST /api/register register
// ---
// summary: Регистрация пользователя.
// description: Эта конечная точка позволяет вам зарегистрироваться на сервере.
// parameters:
// - name: body
//   in: body
//   description: Тело поискового запроса.
//   required: true
//   schema:
//     $ref: '#/definitions/UserAuth'
// responses:
//   '200':
//     description: Токен.
func apiRegister(w http.ResponseWriter, r *http.Request) {
    var claims UserAuth
    err := json.NewDecoder(r.Body).Decode(&claims)
    if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

    if claims.Username == "" || claims.Password == "" {
        http.Error(w, "Username and password are required", http.StatusBadRequest)
        return
    }

    bytes, err := bcrypt.GenerateFromPassword([]byte(claims.Password), 14)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    pass = string(bytes)
    _, str, err := tokenAuth.Encode(map[string]interface{}{"username": claims.Username})
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    w.Header().Set("Content-Type", "application/json")
    err = json.NewEncoder(w).Encode(map[string]string{"token": str})
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
}

// apiLogin handles login requests.
// swagger:operation POST /api/login login
// ---
// summary: Авторизация пользователя.
// description: Эта конечная точка позволяет вам авторизоваться на сервере.
// parameters:
// - name: body
//   in: body
//   description: Тело поискового запроса.
//   required: true
//   schema:
//     $ref: '#/definitions/UserAuth'
// responses:
//   '200':
//     description: Токен.
func apiLogin(w http.ResponseWriter, r *http.Request) {
    var claims UserAuth
    err := json.NewDecoder(r.Body).Decode(&claims)
    if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

    if claims.Username == "" || claims.Password == "" {
        http.Error(w, "Username and password are required", http.StatusBadRequest)
        return
    }

    err = bcrypt.CompareHashAndPassword([]byte(pass), []byte(claims.Password))
    if err != nil {
        http.Error(w, "Invalid password", http.StatusUnauthorized)
        return
    }

    _, str, err := tokenAuth.Encode(map[string]interface{}{"username": claims.Username})
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    t := template.Must(template.New("tmpl").Parse(searchAfterLoginTemplate))

    f, err := os.OpenFile("/app/static/address/search.md", os.O_WRONLY, 0644)
    if err != nil {
        log.Fatal("Error creating file:", err)
        return
    }

    token := Token{
        Token: str,
    }

    err = t.Execute(f, token)
    if err != nil {
        log.Fatal("Error executing template:", err)
        return
    }

    w.Header().Set("Content-Type", "application/json")
    err = json.NewEncoder(w).Encode(map[string]string{"token": str})
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
}

// apiSearchHandler handles search requests.
// swagger:operation POST /api/address/search search
// ---
// summary: Поиск адреса.
// description: Эта конечная точка позволяет вам искать адрес.
// parameters:
// - name: body
//   in: body
//   description: Тело поискового запроса.
//   required: true
//   schema:
//     $ref: '#/definitions/SearchRequest'
// responses:
//   '401':
//     description: Пользователь не авторизован.
//   '403':
//     description: Токен пользователя не валиден.
//   '400':
//     description: Результат геокодирования с неверным форматом запроса.
//   '500':
//     description: Сервис https://dadata.ru не доступен.
//   '200':
//     description: Результаты поиска.
//     schema:
//       $ref: '#/definitions/Address'
func apiSearchHandler(w http.ResponseWriter, r *http.Request) {
    var re SearchRequest
    err := json.NewDecoder(r.Body).Decode(&re)
    if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

    jsonData, err := json.Marshal(re)
    var data = strings.NewReader(string(jsonData))
    client := &http.Client{}
    req, err := http.NewRequest("POST", "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address", data)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Authorization", "Token 864ecfb76388cdeb4ee1f7215e1eb8272f5d56b7")
    resp, err := client.Do(req)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
    defer resp.Body.Close()

    var dadataResponse Address

    err = json.NewDecoder(resp.Body).Decode(&dadataResponse)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    err = json.NewEncoder(w).Encode(dadataResponse)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
}

// apiGeocodeHandler handles geocode requests.
// swagger:operation POST /api/address/geocode geocode
// ---
// summary: Геокодирование адреса.
// description: Эта конечная точка позволяет геокодировать адрес.
// parameters:
// - name: body
//   in: body
//   description: Тело запроса геокодирования.
//   required: true
//   schema:
//     $ref: '#/definitions/GeocodeRequest'
// responses:
//   '401':
//     description: Пользователь не авторизован.
//   '403':
//     description: Токен пользователя не валиден.
//   '400':
//     description: Результат геокодирования с неверным форматом запроса.
//   '500':
//     description: Сервис https://dadata.ru не доступен.
//   '200':
//     description: Результат геокодирования.
//     schema:
//       $ref: '#/definitions/Address'
func apiGeocodeHandler(w http.ResponseWriter, r *http.Request) {
    var re GeocodeRequest
    err := json.NewDecoder(r.Body).Decode(&re)
    if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

    jsonData, err := json.Marshal(re)
    var data = strings.NewReader(string(jsonData))
    client := &http.Client{}
    req, err := http.NewRequest("POST", "https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address", data)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Authorization", "Token 864ecfb76388cdeb4ee1f7215e1eb8272f5d56b7")
    resp, err := client.Do(req)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
    defer resp.Body.Close()

    var dadataResponse Address

    err = json.NewDecoder(resp.Body).Decode(&dadataResponse)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    err = json.NewEncoder(w).Encode(dadataResponse)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
}

type ReverseProxy struct {
    host string
    port string
}

func NewReverseProxy(host, port string) *ReverseProxy {
    return &ReverseProxy{
        host: host,
        port: port,
    }
}

func (rp *ReverseProxy) ReverseProxy(next http.Handler) http.Handler {
    target, _ := url.Parse(fmt.Sprintf("http://%s:%s", rp.host, rp.port))
    proxy := httputil.NewSingleHostReverseProxy(target)
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        if r.URL.Path == "/api/address/search" || r.URL.Path == "/api/address/geocode" || r.URL.Path == "/api/login" || r.URL.Path == "/api/register" || r.URL.Path == "/swagger" {
            next.ServeHTTP(w, r)
        } else {
            proxy.ServeHTTP(w, r)
        }
    })
}
