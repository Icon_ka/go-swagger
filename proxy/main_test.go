package main

import (
    "net/http"
    "testing"
    "net/http/httptest"
    "strings"
)

func TestApiSearchHandler(t *testing.T) {
    payload := `{"query": "москва"}`
    req, err := http.NewRequest("POST", "/api/address/search", strings.NewReader(payload))
    if err != nil {
        t.Fatal(err)
    }

    rr := httptest.NewRecorder()

    apiSearchHandler(rr, req)

    if rr.Code != http.StatusOK {
        t.Errorf("expected status %d but got %d", http.StatusOK, rr.Code)
    }
}

func TestApiGeocodeHandler(t *testing.T) {
    payload := `{"lat": "51.5074", "lon": "-0.1278"}`
    req, err := http.NewRequest("POST", "/api/address/geocode", strings.NewReader(payload))
    if err != nil {
        t.Fatal(err)
    }

    rr := httptest.NewRecorder()

    apiGeocodeHandler(rr, req)

    if rr.Code != http.StatusOK {
        t.Errorf("expected status %d but got %d", http.StatusOK, rr.Code)
    }
}

func Test_apiLogin(t *testing.T) {
    payload := `{"username": "bob", "password": "asdasd"}`
    req, err := http.NewRequest("POST", "/api/register", strings.NewReader(payload))
    if err != nil {
        t.Fatal(err)
    }

    rr := httptest.NewRecorder()
    handler := http.HandlerFunc(apiRegister)
    handler.ServeHTTP(rr, req)

    req, err = http.NewRequest("POST", "/api/login", strings.NewReader(payload))
    if err != nil {
        t.Fatal(err)
    }

    rr = httptest.NewRecorder()
    handler = http.HandlerFunc(apiLogin)
    handler.ServeHTTP(rr, req)
}
